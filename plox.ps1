<#
Current progress: 
    8 Statements and state
    http://craftinginterpreters.com/statements-and-state.html
#>


enum TokenType {
    LEFT_PAREN
    RIGHT_PAREN
    LEFT_BRACE
    RIGHT_BRACE
    COMMA
    DOT
    MINUS
    PLUS
    SEMICOLON
    SLASH
    STAR
    MODULO

    BANG
    BANG_EQUAL
    EQUAL
    EQUAL_EQUAL
    GREATER
    GREATER_EQUAL
    LESS
    LESS_EQUAL

    IDENTIFIER
    STRING
    NUMBER

    AND
    CLASS
    ELSE
    FALSE
    FUN
    FOR
    IF
    NIL
    OR
    PRINT
    RETURN
    SUPER
    THIS
    TRUE
    VAR
    WHILE
    
    EOF
}

enum ExprType {
    NONE
    BINARY
    GROUPING
    LITERAL
    UNARY
}

class Token {
    [TokenType]$type
    [string]$lexeme
    [Object]$literal
    [int]$line

    Token([TokenType]$type, [string]$lexeme, [Object]$literal, [int]$line) {
        $this.type = $type
        $this.lexeme = $lexeme
        $this.literal = $literal
        $this.line = $line
    }

    [string] toString() {
        return "type: $($this.type), lexeme: $($this.lexeme), literal: $($this.literal), line: $($this.line)"
    }
}

class Scanner {
    [string]$source
    [System.Collections.ArrayList] $tokens
    hidden [int] $start
    hidden [int] $current
    hidden [int] $line

    $keywords = @{
        "and" = [TokenType]::AND;
        "class" = [TokenType]::CLASS;
        "else" = [TokenType]::ELSE;
        "false" = [TokenType]::FALSE;
        "for" = [TokenType]::FOR;
        "fun" = [TokenType]::FUN;
        "if" = [TokenType]::IF;
        "nil" = [TokenType]::NIL;
        "or" = [TokenType]::OR;
        "print" = [TokenType]::PRINT;
        "return" = [TokenType]::RETURN;
        "super" = [TokenType]::SUPER;
        "this" = [TokenType]::THIS;
        "true" = [TokenType]::TRUE;
        "var" = [TokenType]::VAR;
        "while" = [TokenType]::WHILE
    }

    Scanner([string]$source) {
        $this.tokens = [System.Collections.ArrayList]::New()
        $this.source = $source
        $this.start = 0
        $this.current = 0
        $this.line = 0
    }

    [System.Collections.ArrayList] scanTokens() {
        while(!$this.isAtEnd()) {
            # we are at the beginning of the next lexeme
            $this.start = $this.current
            $this.scanToken()
        }
        return $null
    }

    [bool] isAtEnd() {
        return $this.current -ge $this.source.Length
    }

    [void] scanToken() {
        $c = $this.advance()
        switch ($c) {
            '(' { $this.addToken([TokenType]::LEFT_PAREN) }
            ')' { $this.addToken([TokenType]::RIGHT_PAREN) }
            '{' { $this.addToken([TokenType]::LEFT_BRACE) }
            '}' { $this.addToken([TokenType]::RIGHT_BRACE) }
            ',' { $this.addToken([TokenType]::COMMA) }
            '.' { $this.addToken([TokenType]::DOT) }
            '-' { $this.addToken([TokenType]::MINUS) }
            '+' { $this.addToken([TokenType]::PLUS) }
            ';' { $this.addToken([TokenType]::SEMICOLON) }
            '*' { $this.addToken([TokenType]::STAR) }
            '%' { $this.addToken([TokenType]::MODULO) }
            '!' { $this.addToken($(if($this.match('=')) {[TokenType]::BANG_EQUAL} else {[TokenType]::BANG})) }
            '=' { $this.addToken($(if($this.match('=')) {[TokenType]::EQUAL_EQUAL} else {[TokenType]::EQUAL})) }
            '<' { $this.addToken($(if($this.match('=')) {[TokenType]::LESS_EQUAL} else {[TokenType]::LESS})) }
            '>' { $this.addToken($(if($this.match('=')) {[TokenType]::GREATER_EQUAL} else {[TokenType]::GREATER})) }
            '/' {
                    if($this.match('/')) {
                        # a comment goes until the end of the line
                        while(($this.peek() -ne "`n") -and (!$this.isAtEnd())) { $this.advance() }
                    } else {
                        $this.addToken([TokenType]::SLASH)
                    }
                }
            ' ' {}
            "`t" {}
            "`r" {}
            "`n" { $this.line++ }
            '"' { $this.lexString() }
            default { 
                if($this.isDigit($c)) {
                    $this.number()
                } elseif($this.isAlpha($c)) {
                    $this.identifier()
                } else {
                    [Lox]::Error($this.line, "", "Unexpected character.")
                }
            }
        }
    }

    [char] peek() {
        if($this.isAtEnd()) { return "`0" }
        return $this.source[$this.current]
    }

    [char] peekNext() {
        if(($this.current+1) -ge $this.source.Length) { return "`0" }
        return $this.source[$this.current+1]
    }

    [bool] isAlpha([char]$c) {
        return ((($c -ge 'a') -and ($c -le 'z')) -or
                (($c -ge 'A') -and ($c -le 'Z')) -or
                ($c -eq '_'))
    }

    [bool] isAlphaNumeric([char]$c) {
        return ($this.isAlpha($c) -or $this.isDigit($c))
    }

    [bool] isDigit([char]$c) {
        return (($c -ge '0') -and ($c -le '9'))
    }

    [void] identifier() {
        while($this.isAlphaNumeric($this.peek())) { $this.advance() }

        $text = $this.source.Substring($this.start, $this.current-$this.start)
        $type = $this.keywords[$text]
        if($type -eq $null) { $type = [TokenType]::IDENTIFIER }

        $this.addToken($type)
    }

    [void] number() {
        while($this.isDigit($this.peek())) {$this.advance()}

        # maybe fractional part
        if($this.peek() -eq '.' -and $this.isDigit($this.peekNext())) {
            $this.advance()

            while($this.isDigit()) {$this.advance()}
        }

        $this.addToken([TokenType]::NUMBER, [double]::Parse($this.source.Substring($this.start, $this.current - $this.start)))
    }

    [void] lexString() {
        while($this.peek() -ne '"' -and !$this.isAtEnd()) {
            if($this.peek -eq "`n") { $this.line++ }
            $this.advance()
        }

        if($this.isAtEnd()) {
            [Lox]::Error($this.line, "", "Unterminated string.")
            return
        }

        $this.advance()

        $val = $this.source.Substring($this.start+1, ($this.current - $this.start - 1))
        $this.addToken([TokenType]::STRING, $val)
    }

    [bool] match([char]$expected) {
        if($this.isAtEnd()) { return $false }
        if($this.source[$this.current] -ne $expected) { return $false }

        $this.current++
        return $true
    }

    [char] advance() {
        return $this.source[$this.current++]
    }

    [void] addToken([TokenType]$type) {
        $this.addToken($type, $null)
    }

    [void] addToken([TokenType]$type, [Object]$literal) {
        $text = $this.source.Substring($this.start, ($this.current - $this.start))
        $t = [Token]::New($type, $text, $literal, $this.line)
        Write-Warning "$($t.toString())"
        $this.tokens.Add($t)
    }
}


class Expr {
    [ExprType]$type

    Expr() {
        $this.type = [ExprType]::NONE
    }

    [string]parenthesize([string]$name, [Object]$expr) {
        $tmp = "( "
        $tmp += $name
        $tmp += " "
        $tmp += $expr.print()
        $tmp += ")"

        return $tmp
    }

    [string]parenthesize([string]$name, [Object]$expr, [Object]$expr2) {
        $tmp = "( "
        $tmp += $name
        $tmp += " "
        $tmp += $expr.print()
        $tmp += " "
        $tmp += $expr2.print()
        $tmp += ")"

        return $tmp
    }
}

class Binary : Expr {
    [Expr]$left
    [Expr]$right
    [Token]$operator

    Binary([Expr]$left, [Token]$operator, [Expr]$right) {
        $this.left = $left
        $this.right = $right
        $this.operator = $operator
        $this.type = [ExprType]::BINARY
    }

    [string] print() {
        return $this.parenthesize($this.operator.lexeme, $this.left, $this.right)
    }
}

class Grouping : Expr {
    [Expr]$expression

    Grouping([Expr]$expression) {
        $this.expression = $expression
        $this.type = [ExprType]::GROUPING
    }

    [string] print() {
        return $this.parenthesize("group", $this.expression)
    }

}

class Literal : Expr {
    [Object]$value

    Literal([Object]$value) {
        $this.value = $value
        $this.type = [ExprType]::LITERAL
    }

    [string] print() {
        if($this.value -eq $null) { return "nil" }
            return $this.value.toString()
    }
}

class Unary : Expr {
    [Token]$operator
    [Expr]$right

    Unary([Token]$operator, [Expr]$right) {
        $this.operator = $operator
        $this.right = $right
        $this.type = [ExprType]::UNARY
    }

    [string] print() {
        return $this.parenthesize($this.operator.lexeme, $this.right)
    }
}


class AstPrinter : Expr {
    [string] print([Object]$expr) {
        return $expr.print()
    }
}

class ParseError : System.Exception {}

class Parser {
    [System.Collections.ArrayList]$tokens
    [int]$current

    Parser([System.Collections.ArrayList]$tokens) {
        $this.tokens = $tokens
    }

    [Expr] parse() {
        try {
            return $this.expression()
        } catch {
            return $null
        }
    }

    [Expr] expression() {
        return $this.equality()
    }

    [Expr] equality() {
        $expr = $this.comparison()

        while($this.match(@([TokenType]::BANG_EQUAL, [TokenType]::EQUAL_EQUAL))) {
            $operator = $this.previous()
            $right = $this.comparison()
            $expr = [Binary]::New($expr, $operator, $right)
        }

        return $expr
    }

    [Expr] comparison() {
        $expr = $this.term()

        while($this.match(@([TokenType]::GREATER, [TokenType]::GREATER_EQUAL, [TokenType]::LESS, [TokenType]::LESS_EQUAL))) {
            $operator = $this.previous()
            $right = $this.term()
            $expr = [Binary]::New($expr, $operator, $right)
        }

        return $expr
    }

    [Expr] term() {
        $expr = $this.factor()

        while($this.match(@([TokenType]::MINUS, [TokenType]::PLUS))) {
            $operator = $this.previous()
            $right = $this.factor()
            $expr = [Binary]::New($expr, $operator, $right)
        }

        return $expr
    }

    [Expr] factor() {
        $expr = $this.unary()

        while($this.match(@([TokenType]::SLASH, [TokenType]::STAR))) {
            $operator = $this.previous()
            $right = $this.unary()
            $expr = [Binary]::New($expr, $operator, $right)
        }

        return $expr
    }

    [Expr] unary() {
        if($this.match(@([TokenType]::BANG, [TokenType]::MINUS))) {
            $operator = $this.previous()
            $right = $this.unary()
            return [Unary]::New($operator, $right)
        }

        return $this.primary()
    }

    [Expr] primary() {
        if($this.match(@([TokenType]::FALSE))) { return [Literal]::New($false) }
        if($this.match(@([TokenType]::TRUE))) { return [Literal]::New($true) }
        if($this.match(@([TokenType]::NIL))) { return [Literal]::New($null) }
        
        if($this.match(@([TokenType]::NUMBER, [TokenType]::STRING))) {
            return [Literal]::New($this.previous().literal)
        }

        if($this.match(@([TokenType]::LEFT_PAREN))) {
            $expr = $this.expression()
            $this.consume([TokenType]::RIGHT_PAREN, "Expected ')' after expression")
            return [Grouping]::New($expr)
        }
        throw error($this.peek(), "Expected expression")
        # return $null
    }

    [bool] match([TokenType[]]$types) {
        foreach($type in $types) {
            if($this.check($type)) {
                $this.advance()
                return $true
            }
        }

        return $false
    }

    [Token] consume([TokenType]$type, [string]$message) {
        if($this.check($type)) { return $this.advance() }

        throw error($this.peek(), $message)
    }

    [bool] check([TokenType]$type) {
        if($this.isAtEnd()) { return $false }
        return $this.peek().type -eq $type
    }

    [Token] advance() {
        if(!$this.isAtEnd()) { $this.current++ }
        return $this.previous()
    }

    [bool] isAtEnd() {
        return $this.peek().type -eq [TokenType]::EOF
    }

    [Token] peek() {
        return $this.tokens[$this.current]
    }

    [Token] previous() {
        return $this.tokens[$this.current-1]
    }

    [ParseError] error([Token]$token, [string]$message) {
        [Lox]::Error($token, $message)
        return [ParseError]::New()
    }

    [void] synchronize() {
        $this.advance()

        while(!$this.isAtEnd()) {
            if($this.previous() -eq [TokenType]::SEMICOLON) { return }

            switch($this.peek().type) {
                [TokenType]::CLASS { return }
                [TokenType]::FUN { return }
                [TokenType]::VAR { return }
                [TokenType]::FOR { return }
                [TokenType]::IF { return }
                [TokenType]::WHILE { return }
                [TokenType]::PRINT { return }
                [TokenType]::RETURN { return }
            }

            $this.advance()
        }
    }
}

class Interpreter : Expr {

    [void] interpret([Expr]$expression) {
        try {
            $value = $this.evaluate($expression)
            Write-Host $this.stringify($value)
        } catch {
            [Lox]::runtimeError($error)
        }
    }
    
    [Object] visitLiteralExpr([Literal]$expr) {
        return $expr.value
    }

    [Object] visitGroupingExpr([Grouping]$expr) {
        return $this.evaluate($expr.expression)
    }

    [Object] visitUnaryExpr([Unary]$expr) {
        $right = $this.evaluate($expr.right)

        switch($expr.operator.type) {
            [TokenType]::BANG { return !$this.isTruthy($right) }
            [TokenType]::MINUS { 
                $this.checkNumberOperands($expr.oprator, $right)
                return -[double]$right 
            }
        }

        return $null
    }

    [void] checkNumberOperands([Token] $operator, [Object] $operand) {
        if($operand -is [double]) { return }
        throw error($operator, "Operand must be a number")
    }

    [Object] visitBinaryExpr([Binary]$expr) {
        $left = $this.evaluate($expr.left)
        $right = $this.evaluate($expr.right)

        switch($expr.operator.type) {
            GREATER { 
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left -gt [double]$right
            }
            GREATER_EQUAL { 
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left -ge [double]$right
            }
            LESS {
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left -lt [double]$right
            }
            LESS_EQUAL {
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left -le [double]$right
            }
            MINUS {
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left -lt [double]$right
            }
            PLUS {
                if(($left -is [double]) -and ($right -is [double])) {
                    return [double]$left + [double]$right
                }
                if(($left -is [string]) -and ($right -is [string])) {
                    return [string]$left + [string]$right
                }
                throw error($expr.operator, "Operands must be two numbers or two strings")
            }
            SLASH {
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left / [double]$right
            }
            STAR {
                $this.checkNumberOperands($expr.operator, $left, $right)
                return [double]$left * [double]$right
            }
            default {
                Write-Host "hello fail"
            }
        }

        Write-Host "hello binary expr"

        return $null
    }

    [void] checkNumberOperands([Token] $operator, [Object] $left, [Object] $right) {
        if(($left -is [double]) -and ($right -is [double])) { return }
        throw error($operator, "Operands must be numbers")
    }

    [Object] evaluate([Expr]$expr) {

        switch($expr.type) {
            BINARY { return $this.visitBinaryExpr($expr) }
            GROUPING { return $this.visitGroupingExpr($expr) }
            LITERAL { return $this.visitLiteralExpr($expr) }
            UNARY { return $this.visitUnaryExpr($expr) }
            default { return $null }
        }

        return $null
    }

    [bool] isTruthy([Object] $obj) {
        if($obj -eq $null) { return $false }
        if($obj -is [bool]) { return [Boolean]$obj }
        return $true
    }

    [bool] isEqual([Object] $a, [Object] $b) {
        if((ba -eq $null) -and ($b -eq $null)) { return $true }
        if($a -eq $null) { return $false }

        return ($a -eq $b)
    }

    [string] stringify([Object] $obj) {
        if($obj -eq  $null) { return "nil" }

        if($obj -is [double]) {
            $text = $obj.ToString()
            if($text.EndsWith(".0")) {
                $text = $text.Substring(0, $text.Length-2)
            }

            return $text
        }

        return $obj.ToString()
    }
}

class Lox {
    static [bool] $had_error
    static [bool] $had_runtime_error
    static [Interpreter] $interpreter

    Lox() {
        [Lox]::had_error = $false
        [Lox]::had_runtime_error = $false
        [Lox]::interpreter = [Interpreter]::New()
    }

    static [void] error([Token]$token, [string]$message) {
        if($token.type == ([TokenType]::EOF)) {
            [Lox]::report($token.line, " at end", $message)
        } else {
            [Lox]::report("$($token.line) at '$($token.lexeme)'", $message)
        }
    }

    static [void] report([int]$line, [string]$where, [string]$message) {
        Write-Error "[line $line] Error $where : $message"
        [Lox]::had_error = $true
    }

    static [void] runtimeError($error) {
        Write-Host "[line $($error.token.line)]"
        [Lox]::had_runtime_error = $true
    }

    <#
    static [void] Error ([int]$line, [string]$where, [string]$message) {
        Write-Error "[line $line] Error $where : $message"
        [Lox]::had_error = $true
    }
    #>

    [void] run ([string]$source) {
        # [Lox]::Error(15, "run()", "print out tokens")

        $scn = [Scanner]::New($source)
        $tokens = [System.Collections.ArrayList]::New()

        $scn.scanTokens()

        $parser = [Parser]::New($scn.tokens)
        do {
            $expression = $parser.parse()

            if([Lox]::had_error) { return }
            if($expression -ne $null) {
                [Lox]::interpreter.interpret($expression)
                Write-Host $expression.print()
            }
        } while($expression -ne $null)
    }

    [void] runFile ([string]$filename) {
        $source = Get-Content -Raw $filename
        $cnt = 0
        
        $this.run($source)
        if([Lox]::had_error) { throw "Exiting after Error" }
        if([Lox]::had_runtime_error) { throw "Exiting after runtime Error" }
    }

    [void] runPrompt() {
        while($true) {
            Write-Host -NoNewline "> "
            $line = Read-Host
            if($line -eq "") {
                break
            }
            $this.run($line)
            [Lox]::had_error = $false
        }
    }

}

Function main {
    [cmdletbinding()]
    param (
        [Parameter(Mandatory=$false)]
        [string]$script
    )

    [Lox] $lx = [Lox]::New()

    if($script) {
        $lx.runFile($script)
    } else {
        $lx.runPrompt()
    }
}

# Write-Host "Scanning file hello_lox.txt..."
# main ".\hello_lox.txt"
# Write-Host "Scanning file scanner.txt..."
main ".\scanner.txt"
# main

<#
$expression = [Binary]::New(
    [Unary]::New(
        [Token]::New([TokenType]::MINUS, "-", $null, 1),
        [Literal]::New(28950)),
    [Token]::New([TokenType]::STAR, "*", $null, 1),
    [Grouping]::New(
        [Literal]::New(392.4)
    )
)

Write-Host $expression.print()
#>